var http = require('http'),
  fs = require('fs'),
  tj = require('@mapbox/togeojson'),
  process = require('child_process'),
  async = require('async'),
  DOMParser = require('xmldom').DOMParser,
  request = require('request'),
  unzipper,
  unzip = function (file, destination) { //destination is a directory
            unzipper = process.exec('unzip -qn ' + file + ' -d ' + destination);
          };

http.createServer(function (req, res) {
  async.waterfall([
    function(callback) {
        unzip ('Week7_to_Week6quarry.kmz', '/home/mkostman/s3tests/unzipped/')
        callback(null, '/home/mkostman/s3tests/unzipped/');
    },
    function(arg1, callback) {
        //Search for the file with kml in name
        var fileList = [];
        var files = fs.readdirSync('/home/mkostman/s3tests/unzipped/');
        console.log('files', files);
        callback(null, files);
    },
    function(arg1, callback) {
        var kmlList = [];
        for (var i = 0; i < arg1.length; i++){
          if (arg1[i].includes('kml')){
            kmlList.push(arg1[i]);
          }
        }
        console.log('kmlList', kmlList);
        callback(null, kmlList);
    },
    function(arg1, callback) {
        var file = fs.readFileSync('/home/mkostman/s3tests/unzipped/' + arg1[0], 'utf8');
        console.log('file', file);
        var kml = new DOMParser().parseFromString(file);
        console.log('kml', kml);
        callback(null, kml);
    },
    function(arg1, callback) {
        var convertedWithStyles = tj.kml(arg1, { styles: true });
        console.log('converted', convertedWithStyles)
        callback(null, convertedWithStyles);
    }
], function (err, result) {
    res.end(JSON.stringify(result));
});



}).listen(1338, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1338/');
